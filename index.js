// Copier-coller de la liste de film
const movieList = [
  {
    title: "Avatar",
    releaseYear: 2009,
    duration: 162,
    director: "James Cameron",
    actors: [
      "Sam Worthington",
      "Zoe Saldana",
      "Sigourney Weaver",
      "Stephen Lang",
    ],
    description:
      "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
    rating: 7.9,
  },
  {
    title: "300",
    releaseYear: 2006,
    duration: 117,
    director: "Zack Snyder",
    actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
    description:
      "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
    rating: 7.7,
  },
  {
    title: "The Avengers",
    releaseYear: 2012,
    duration: 143,
    director: "Joss Whedon",
    actors: [
      "Robert Downey Jr.",
      "Chris Evans",
      "Mark Ruffalo",
      "Chris Hemsworth",
    ],
    description:
      "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    rating: 8.1,
  },
  {
    title: "Spider-Man - Accross the Spider-Verse",
    releaseYear: 2023,
    duration: 140,
    director: "Joaquim Dos Santos, Kemp Powers, Justin K. Thompson",
    actors: ["Shameik Moore", "Hailee Steinfeld", "Jake Johnson"],
    description:
      "An animated superhero film that follows Miles Morales as he embarks on a multiversal adventure, teaming up with different Spider-People to confront new threats and challenges across parallel dimensions.",
    poster:
      "https://www.amazon.fr/Poster-Spider-Man-Marvel-Comics-Spider-Verse/dp/B0C5YFL6FT",
    rating: 9.1,
  },
];

// Recherche de l'index du film "300"
const indexToRemove = movieList.findIndex((movie) => movie.title === "300");

// Suppression du film à l'index trouvé
if (indexToRemove !== -1) {
  movieList.splice(indexToRemove, 1);
}

// Afficher chaque film avec une boucle avec toutes les infos
for (let i = 0; i < movieList.length; i++) {
  const movie = movieList[i];
  console.log(`Film ${i + 1}:`);
  console.log(`Titre: ${movie.title}`);
  console.log(`Année de sortie: ${movie.releaseYear}`);
  console.log(`Durée: ${movie.duration} minutes`);
  console.log(`Réalisateur: ${movie.director}`);
  console.log(`Acteurs: ${movie.actors.join(", ")}`);
  console.log(`Description: ${movie.description}`);
  console.log(`Note: ${movie.rating}`);
  console.log(`Affiche: ${movie.poster}`);
  console.log("\n");
}
